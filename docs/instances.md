---
title: Instances
permalink: /instances
nav_order: 7
---

## Instances

- [gancio.cisti.org](https://gancio.cisti.org) (Turin, Italy)
- [lapunta.org](https://lapunta.org) (Florence, Italy)
- [sapratza.in](https://sapratza.in/) (Sardinia, Italy)
- [ponente.rocks](https://ponente.rocks) (Ponente Ligure, Italy)
- [puntello.org](https://puntello.org) (Milan, Italy)
- [bcn.convoca.la](https://bcn.convoca.la/) (Barcelona)
- [bonn.jetzt](https://bonn.jetzt/) (Digital-Events aus Bonn, Rhein-Sieg und der Region)
- [quest.livellosegreto.it](https://quest.livellosegreto.it/)
- [ezkerraldea.euskaragendak.eus](https://ezkerraldea.euskaragendak.eus/)
- [lakelogaztetxea.net](https://lakelogaztetxea.net)
- [agenda.eskoria.eus](https://agenda.eskoria.eus/)
- [lubakiagenda.net](https://lubakiagenda.net/)



<small>Do you want your instance to appear here? [Write us]({% link contact.md %}).</small>
